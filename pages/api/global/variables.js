
const CATEGORIES = [
    "Experiences and Destinations",
    "Online Experiences",
    "Wool",
    "Naturally Dyed",
    "Natural Cosmetics",
    "Vegetal Fibers"
];

module.exports = {
    CATEGORIES
}