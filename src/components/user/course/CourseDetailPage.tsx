import { useTranslations } from "next-intl";
import Image from "next/image"
import { recentOnlineExperiences, recentDestinations, recentLIVE } from "../../temp"

import {
  Wrapper,
  CourseContainer,
  Container,
  Icon,
  Button,
  CourseCard,
} from "../../ui";
import { IoPlayCircleOutline } from "react-icons/io5";
import ReactPlayer from "react-player";
import { VAR_STR_LIVE, VAR_STR_ONLINE } from "@/src/functions";
import { FC } from "react";

export const CourseDetailPage:FC<{buy:any}> = ({buy}) => {
  
  return (
    <>
      {buy !== "" ? 
        <DetailCourseBuy />:
        <DetailCourse/>
      }      
    </>
  );
}

const DetailCourseBuy = () => {
  const epsodes = 9;
  return (
    <>
      <Wrapper>
        <Container>
          <div className="flex  flex-col justify-start">
            <h3 className="mb-10">Title of Experiences</h3>

            <div id="accordionExample">
              <div
                className="rounded-t-lg border border-neutral-200 bg-white dark:border-neutral-600 dark:bg-neutral-800">
                <h2 className="mb-0" id="headingOne">
                  <button
                    className="group relative flex w-full items-center rounded-t-[15px] border-0 bg-white py-4 px-5 text-left text-base text-neutral-800 transition [overflow-anchor:none] hover:z-[2] focus:z-[3] focus:outline-none dark:bg-neutral-800 dark:text-white [&:not([data-te-collapse-collapsed])]:bg-white [&:not([data-te-collapse-collapsed])]:text-primary [&:not([data-te-collapse-collapsed])]:[box-shadow:inset_0_-1px_0_rgba(229,231,235)] dark:[&:not([data-te-collapse-collapsed])]:bg-neutral-800 dark:[&:not([data-te-collapse-collapsed])]:text-primary-400 dark:[&:not([data-te-collapse-collapsed])]:[box-shadow:inset_0_-1px_0_rgba(75,85,99)]"
                    type="button"
                    data-te-collapse-init
                    data-te-target="#collapseOne"
                    aria-expanded="true"
                    aria-controls="collapseOne">
                    Episoida 1                    
                  </button>
                </h2>
                <div
                  id="collapseOne"
                  className="!visible"
                  data-te-collapse-item
                  data-te-collapse-show
                  aria-labelledby="headingOne"
                  data-te-parent="#accordionExample">
                  <div className="py-4 px-5 flex flex-col md:flex-row gap-5 md:gap-10">
                    <div id="video">
                      <CourseCard className="course-card w-[300px]" kind={VAR_STR_LIVE} course={recentOnlineExperiences[0]} />
                    </div>
                    <div id="description" className="">
                      It is
                      shown by default, until the collapse plugin adds the appropriate
                      classes that we use to style each element. These classes control
                      the overall appearance, as well as the showing and hiding via CSS
                      transitions. You can modify any of this with custom CSS or
                      overriding our default variables.
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="border border-t-0 border-neutral-200 bg-white dark:border-neutral-600 dark:bg-neutral-800">
                <h2 className="mb-0" id="headingTwo">
                  <button
                    className="group relative flex w-full items-center rounded-none border-0 bg-white py-4 px-5 text-left text-base text-neutral-800 transition [overflow-anchor:none] hover:z-[2] focus:z-[3] focus:outline-none dark:bg-neutral-800 dark:text-white [&:not([data-te-collapse-collapsed])]:bg-white [&:not([data-te-collapse-collapsed])]:text-primary [&:not([data-te-collapse-collapsed])]:[box-shadow:inset_0_-1px_0_rgba(229,231,235)] dark:[&:not([data-te-collapse-collapsed])]:bg-neutral-800 dark:[&:not([data-te-collapse-collapsed])]:text-primary-400 dark:[&:not([data-te-collapse-collapsed])]:[box-shadow:inset_0_-1px_0_rgba(75,85,99)]"
                    type="button"
                    data-te-collapse-init
                    data-te-collapse-collapsed
                    data-te-target="#collapseTwo"
                    aria-expanded="false"
                    aria-controls="collapseTwo">
                    Episodia 2                    
                  </button>
                </h2>
                <div
                  id="collapseTwo"
                  className="!visible hidden"
                  data-te-collapse-item
                  aria-labelledby="headingTwo"
                  data-te-parent="#accordionExample">
                  <div className="py-4 px-5 flex flex-col md:flex-row">
                    <div id="video">
                      <ReactPlayer className="rounded-3xl" />
                    </div>
                    <div id="description" className="">
                      It is
                      shown by default, until the collapse plugin adds the appropriate
                      classes that we use to style each element. These classes control
                      the overall appearance, as well as the showing and hiding via CSS
                      transitions. You can modify any of this with custom CSS or
                      overriding our default variables.
                    </div>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </Container>
      </Wrapper>
    </>
  )
}

const DetailCourse = () => {
  const t = useTranslations("user.course");
  return (
    <>
      <Wrapper>
        <Container className="w-full mx-auto">
          <div className="flex flex-col md:flex-row w-full gap-10 items-start course-detail">
            <div className="relative">
              <ReactPlayer className="rounded-3xl overflow-hidden"
                url='https://tecdn.b-cdn.net/img/video/Tropical.mp4'
                // light={true}
                loop={true}
                controls={true}
              />
            </div>
            <div className="flex flex-col h-fit gap-5">
              <h4 className="">
                Name of the Course
              </h4>
              <span className="">Destination and Country</span>

              <div className="flex flex-row items-center justify-left space-x-2 py-1">
                <Image
                  src="/image/icon/experiences.png"
                  blurDataURL={"/image/icon/experiences.png"}
                  width="24"
                  height="24"
                  alt="ICON"
                  placeholder="blur"
                />
                <span className="">Artisan name</span>
                <span className="w-full text-end">$95 USD</span>
              </div>
              <div className="flex flex-row gap-10">
                <span className="">Episode: {0}</span>
                <span className="">Total Duration: {0}</span>
              </div>
              <div className="flex flex-row gap-10">
                <span className="">categories</span>
                <span className="">categories</span>
              </div>

              <Button className="mt-5" label="Buy" />
            </div>

          </div>
        </Container>
      </Wrapper>
      <Wrapper>
        <Container>
          <h3 className="w-full subtitle">
            {t("subtitle.online-experiences")}
          </h3>
          <CourseContainer courses={recentOnlineExperiences} kind={VAR_STR_ONLINE} />
        </Container>
      </Wrapper>
    </>
  )
}
