import React, { useState } from 'react';
import DatePicker from 'react-tailwindcss-datepicker';

interface DateRangeType {
  startDate: Date;
  endDate: Date;
}

export const DateRangePicker = () => {

  const [dateRange, setDateRange] = useState<DateRangeType>({
    startDate: new Date(),
    endDate: new Date(),
  });

  const handleChange = (newValue: any) => {
    setDateRange(newValue)
  }

  return (
    <div className="w-full custom-date-picker">
      <DatePicker
        i18n="es"
        showFooter={true}
        primaryColor={"orange"}
        useRange={true}
        value={dateRange}
        onChange={handleChange}
      />
    </div>
  );
};