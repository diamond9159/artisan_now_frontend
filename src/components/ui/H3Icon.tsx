import { Children, FC } from "react";
import Image from "next/image"

type Props = {
    icon?: any; 
    size?: string;
    children: string;
    descrpition?: string;
};

export const H3Icon: FC<Props> = (props) => {
    const { children, icon, size = "48px", descrpition } = props;
    return (
        <div className="flex flex-row items-center">
            {icon &&
                <div className={`mr-2 w-[${size}] h-[${size}]`}>
                    <Image src={icon} alt={""} style={{width:"100%", height:"100%"}} />
                </div>
            }
            <div className="flex flex-col">
                <h3 className="w-full subtitle">
                    {children}
                </h3>
                {descrpition && <span className="mb-5">{descrpition}</span>}
            </div>
            
        </div>        
    );
};
