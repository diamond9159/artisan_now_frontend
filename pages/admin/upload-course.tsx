import { DefaultLayout } from "@/src/components/layout/default";
import { NextPage } from "next";
import CourseUpload from "@/src/components/admin/dashboard/CourseUpload";

const uploadCourse: NextPage = () => {
  return (
    <DefaultLayout>
      <CourseUpload />
    </DefaultLayout>
  );
};

export default uploadCourse;