import Image from "next/image"
import { PlayIconUrl } from "./customIcons"
export const PlayIcon = () => {
    return (
        <div className="cursor-pointer absolute right-5 bottom-5 z-10 bg-white rounded-md">
            <Image
                src={PlayIconUrl}
                width="32"
                height="32"
                alt="ICON"
            />
        </div>
    )
}