import { Button } from "./Button"

export const NothingCourse = () => {
    return (
        <div className="flex flex-col max-w-md">
            <h4 className="mt-10">Todavía no tienes ninguna experiencia comprada</h4>
            <label className="my-5">
                Conéctate, impacta positivamente en la economía de artesanas de Latinoamérica y aprende viviendo una experiencia auténtica
            </label>
            <Button label="Comienza a buscar" />
        </div>
    )

 }