const g_var = require("../global/variables");

function initialRole(Role) {
    Role.estimatedDocumentCount().then((count, err) => {
        if (!err && count === 0) {
            new Role({
                name: "user"
            }).save().then((res, err) => {
                if (!res) {
                    console.log("error", err);
                }
                console.log("added 'user' to roles collection");
            });

            new Role({
                name: "artisan"
            }).save().then((res, err) => {
                if (!res) {
                    console.log("error", err);
                }

                console.log("added 'artisan' to roles collection");
            });

            new Role({
                name: "admin"
            }).save().then((res, err) => {
                if (!res) {
                    console.log("error", err);
                }

                console.log("added 'admin' to roles collection");
            });
        }
    });
}

function initialCategories(Categories) {
    
    Categories.estimatedDocumentCount().then((count, err) => {
        if (!err && count === 0) {
            g_var.CATEGORIES.map((item) => { 
                new Categories({
                    name: item
                }).save().then((res, err) => {
                    if (!res) {
                        console.log("error", err);
                        return;
                    }
                    console.log(`added ${item} to categories collection`);
                });
            })            
              
        }
    })
}

const g_func = {
    initialRole,
    initialCategories,
};
module.exports = g_func;