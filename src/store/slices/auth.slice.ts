import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AuthState, PERMISSION_TYPE, User } from "@/src/functions/types";

const initialState: AuthState = {
  user: null,
  token: null,
  loggingIn: false,
  loggedIn: false,
  registering: false,
  registered: false,
  role: null,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    loginRequest(state) {
      state.loggingIn = true;
      state.registered = false;
    },
    loginSuccess(state, action: PayloadAction<{user:User, role:PERMISSION_TYPE}>) {
      state.token = action.payload.user.token;
      state.role = action.payload.role;
      state.user = action.payload.user;
      state.loggingIn = false;
      state.loggedIn = true;
    },
    switchToRoleSuccess(state, action:PayloadAction<PERMISSION_TYPE>) {
      state.role = action.payload
    },
    loginFailure(state) {
      state.loggingIn = false;
      state.user = null;
    },
    logoutSuccess() {
      return initialState;
    },
    registerRequest(state) {
      console.log("here");
      state.registering = true;
    },
    registerSuccess(state) {
      state.registering = false;
      state.registered = true;      
    },
    registerFailure(state) {
      state.registering = false;
      state.user = null;
    }
  },
});

export const { registerRequest, registerSuccess, registerFailure } =
  authSlice.actions;

export const { loginRequest, loginSuccess, loginFailure, logoutSuccess, switchToRoleSuccess } =
  authSlice.actions;

export const authSelector = (state: AuthState) => state
export const authLoggingIn = (state: AuthState) => state.loggingIn

export default authSlice.reducer;

