const authJwt = require("./authJwt");
const verifySignUp = require("./verifySignUp");
const videoUpload = require("./video.upload");

module.exports = {
    authJwt,
    verifySignUp,
    videoUpload
};