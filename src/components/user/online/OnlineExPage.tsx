import { courseInfo, VAR_STR_DESTINATION, VAR_STR_LIVE, VAR_STR_ONLINE } from "@/src/functions";
import { useTranslations } from "next-intl";
import lives from "@/public/course.json"
import { recentLIVE, recentOnlineExperiences, MyOnlineExperiences } from "../../temp"

import { Wrapper, CourseContainer, Container, LiveCourseContainer, Button } from "../../ui";
import { NothingCourse } from "../../ui/NothingCourse";

export function OnlineExPage() {
  const t = useTranslations("user.online");
  const courses: courseInfo[] = [];
  return (
    <>
      <Wrapper>
        <Container>
          <h3 className="w-full subtitle">
            {t("subtitle.my-online-experiences")}
          </h3>
          {MyOnlineExperiences.length ? <CourseContainer buy="923y03" courses={MyOnlineExperiences} kind={ VAR_STR_ONLINE } /> :
            <NothingCourse />
          }
        </Container>
      </Wrapper>      
      <Wrapper>
        <Container>
          <h3 className="w-full subtitle">
            {t("subtitle.upcoming-live-experiences")}
          </h3>
          <CourseContainer courses={recentLIVE} kind={VAR_STR_LIVE} />
        </Container>
      </Wrapper>
      <Wrapper>
        <Container>
          <h3 className="w-full subtitle">
            {t("subtitle.recommended-for-you")}
          </h3>
          {recentOnlineExperiences.length ?
            <CourseContainer courses={recentOnlineExperiences} kind={VAR_STR_ONLINE} /> :
            <NothingCourse />
          }
        </Container>
      </Wrapper>
    </>
  );
}
