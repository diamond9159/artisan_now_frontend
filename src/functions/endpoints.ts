

const endpoints = {
    authSignIn: `/api/auth/signin`,
    authSignUp: `/api/auth/signup`,
    
    uploadCourse: `/api/upload/course`,
}

export { endpoints };