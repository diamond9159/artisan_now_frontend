import { homeUrl } from "@/src/functions";
import { logOut } from "@/src/store";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";

const Logout: NextPage = () => {

    const dispatch = useDispatch();
    const router = useRouter();
    
    dispatch<any>(logOut())
    router.push(homeUrl);

    return (
        <>
        </>
    )

};

export default Logout;