import { useTranslations } from "next-intl";
import { RiSearchLine } from "react-icons/ri";
import Image from "next/image"

import { Wrapper, CategoriesBar, CourseContainer, Container, Button, Trips } from "../../ui";
import { NothingCourse } from "../../ui/NothingCourse";

export function TripsPage() {
  const t = useTranslations("user.trips");
  const data = false;
  return (
    <>
      <Wrapper>
        <Container>
          <div className="w-full items-start justify-center mx-auto">
            <h3 className="mb-10"> Next trip confirmed</h3>
            { !data &&
              <NothingCourse />
            }
            { data && 
              <div className="flex flex-col justify-start gap-10">
                <Trips />
                <Trips />
                <Trips />
              </div>
            }
          </div>
        </Container>
      </Wrapper>      
    </>
  );
}
