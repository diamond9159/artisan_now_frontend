import { PERMISSION_TYPE, User } from "@/src/functions/types";
import { AppDispatch } from "..";
import { authService } from "../services";
import { loginFailure, loginRequest, loginSuccess, logoutSuccess, registerFailure, registerRequest, registerSuccess, switchToRoleSuccess } from "../slices";

export const logIn = (username: string, password: string, role: PERMISSION_TYPE) => async (
    dispatch: AppDispatch
) => {
    try {
        dispatch(loginRequest());
        const user = await authService.login(username, password);
        console.log({user})
        dispatch(loginSuccess( {user, role} ));
    } catch (error) {
        dispatch(loginFailure());
    }
};

export const logOut = () => async (
    dispatch: AppDispatch
) => {
    try {
        dispatch(logoutSuccess());
    } catch (error) {
    }
};

export const switchToRole = (role: PERMISSION_TYPE) => async(
    dispatch: AppDispatch
) => {
    dispatch(switchToRoleSuccess(role))
}

export const register = (newUser: User) => async (
    dispatch: AppDispatch
) => {
    try {
        dispatch(registerRequest());
        await authService.register(newUser);
        dispatch(registerSuccess());
    } catch (error) {
        dispatch(registerFailure());
    }
};