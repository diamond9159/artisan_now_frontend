import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // Add this import
import { authSlice, usersSlice, localeSlice, courseSlice } from './slices';

export * from './slices';
export * from "./actions";

const persistConfig = {
    key: 'root',
    storage: storage,
};

const rootReducer = combineReducers({
    locales: localeSlice.reducer,
    course: courseSlice.reducer,
    auth: authSlice.reducer,
    users: usersSlice.reducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
    reducer: persistedReducer,
});

export const persistor = persistStore(store);
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof rootReducer>;
