export const courses = [
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    }
]

export const recentOnlineExperiences = [
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    },
]

export const recentDestinations = [
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "price": "20"
    }, {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "price": "20"
    }, {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "price": "20"
    }, {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "price": "20"
    },
]

export const recentLIVE = [
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "price": "20"
    }, {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "price": "20"
    }, {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "price": "20"
    }, {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "price": "20"
    },
]

export const MyOnlineExperiences = [
    {
        "imageUrl": "/image/card/5043-original.jpg",
        "courseName": "Title of experiences",
        "hostName": "Name of hostess or artisan",
        "destination": "Destination",
        "episode": 0,
        "duration": 0,
        "categories": ["category1", "category2"],
        "price": "20"
    }
]