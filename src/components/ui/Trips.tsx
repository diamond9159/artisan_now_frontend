import Image from "next/image"
import { Button } from "./Button"

export const Trips = () => {
    return (
        <div className="w-fit flex flex-col">
        <div className="flex flex-row gap-5">
            <div className="w-[300px] h-[170px]">
                <Image className="rounded-xl" src="/test.webp" alt="" width={600} height={400} style={{ width: "100%", height: "100%" }} />
            </div>
            <div className="flex flex-col">
                <h5>Title of the experiences</h5>
                <span className="light">Name of the hostess or local expert</span>
                <span className="light">Travel dates <italic>(ex.Mar 15, 2022 - Apr 14, 2022)</italic></span>
                <div className="flex justify-end mt-5">
                    <Button label="More Details" />
                </div>
            </div>
        </div>
        </div>
    )
}