
const { g_var } = require('../../global');
const { videoUpload } = require('../../middleware');
const Course = require('../../models');

exports.courseUpload = (req, res) => {
    try {        
       
        // Extract video metadata from request body
        // videoUpload.fields([
        //     { name: 'prevVideoFile', maxCount: 1 },
        //     { name: 'realVideoFile', maxCount: 1 }
        // ]),
        // const { title, hostname, dest, duration } = req.body;
        // const category = g_var.CATEGORIES[0];
        // // Create a new video object with the extracted metadata and uploaded file data
        /*const newCourse = new Course({
            title,
            hostname,
            dest,
            category,
            duration,
            prevVideoFile,
            realVideoFile,
            uploadDate,            
        });
        */
        // Save the new Course object to the database
        // newCourse.save().then((res) => {
        //     // Send a success response back to the client
        //     res.status(201).json({ message: 'Course uploaded successfully', res });
    
        // }).catch((err) => {
        //     res.status(500).json({ message: err.message });
        // });

        //res.status(201).json({ message: 'Course uploaded successfully', res });
        res.redirect("../../admin/upload-course");
    } catch (error) {
        console.error(error.message);
        res.status(500).json({ message: error.message });
    }
};

exports.courseUploadTest = (req, res) => { 
    res.status(200).json({ message: 'Course test' });
}
