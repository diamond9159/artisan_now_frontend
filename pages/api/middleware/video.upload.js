const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'storage/uploads/')
    },
    filename: (req, file, cb) => {
        const uuid = req?.body?.uuid;
        cb(null, uuid + "_" + file.fieldname + path.extname(file.originalname))
    }
});

const videoUpload = multer({ storage: storage });

module.exports = videoUpload;




