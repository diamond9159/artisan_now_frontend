const authRoutes = require("./auth.routes");
const userRoutes = require("./user.routes");
const uploadRoutes = require("./upload.routes");

module.exports = {
    authRoutes,
    userRoutes,
    uploadRoutes
};