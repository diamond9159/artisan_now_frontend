import { authService } from "../services";

export const getUsers = async () => {
    const users = await authService.getAll();
    return users;
};

export const deleteUser = async (id: number) => {
    await authService.delete(id);
    return id;
};