import ReactPlayer from "react-player"
import { Button, Container, ImageButton, Wrapper } from "../../ui"


export const PlayCoursePage = () => {
    return (
        <>
            <Wrapper>
                <Container>
                    <div className="flex flex-col gap-10">
                        <h2>
                            Title of Experiences
                        </h2>
                        <div>
                            <ReactPlayer />
                        </div>
                        <div>
                            <Button label="Back" /> 
                        </div>
                    </div>
                </Container>
            </Wrapper>
        </>
    )
}