// authService.ts
import { authHeader } from '../helpers/auth-header';
import { toast } from 'react-toastify';
import { endpoints } from '@/src/functions';

function uploadCourse(data: FormData) {
  const requestOptions: RequestInit = {
    method: "POST",
    headers: {
      ...authHeader, // This spread operator is used since authorizationHeader will contain 'Authorization' key only when user is loggedin.
    },
    body: data,
  };

  return fetch(endpoints.uploadCourse, requestOptions).then(handleResponse).then((resp) => {
    toast.success("Upload course successfully!");
    console.log("courseResp:= ",resp);    
  });
}


async function handleResponse(response: Response) {

  const isJson = response.headers?.get('content-type')?.includes('application/json');
  const data = isJson ? await response.json() : null;
  if (!response.ok) {
    if ([500, 401, 403, 400].includes(response.status)) {
   
    }    
    const err = data? data.message : response.statusText;  
    toast.error(err)  
    return Promise.reject(err);
  }

  return data;
}

export const uploadService = {
  uploadCourse
};

