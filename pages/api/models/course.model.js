const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
    title: { type: String, required: true },
    hostname: { type: String, required: true },
    dest: { type: String, required: true },
    category: { type: String, required: true },
    episodes: { type: [String], required: false },
    duration: { type: Number, required: true },
    categories: { type: [String], required: true },
    prevVideoUrl: { type: String, required: true },
    realVideoUrl: { type: String, required: true },
    uploadDate: { type: Date, default: Date.now },
});

const Course = mongoose.model("Course", courseSchema);

module.exports = Course;