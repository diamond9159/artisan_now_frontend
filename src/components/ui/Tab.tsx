import React from "react";

type Props = {
  label: string;
  action: () => void;
  isSelected: boolean;
  direction?: string;
  fontsize?: string;
};

export const Tab: React.FC<Props> = ({ label, action, direction = "horitical", isSelected, fontsize = "text-2xl" }) => {
  
  const getClasses = (selected: boolean) =>
    `w-fit border-brown-200 hover:border-brown-600 px-3 py-2 ${" "}
    ${direction == "vertical" ? "border-b-2" : "border-b-2"} ${" "}
    ${fontsize} ${" "}
    ${selected ? "border-brown-600" : "border-brown-100/[.3]"}`;
  
  return (
    <button onClick={action} className={getClasses(isSelected)}>
      {label}
    </button>
  );
}