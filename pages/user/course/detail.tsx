import { DefaultLayout } from "@/src/components/layout/default";
import {CourseDetailPage, TouristDetailPage, LiveDetailPage} from "@/src/components/user/course";
import { VAR_STR_DESTINATION, VAR_STR_LIVE, VAR_STR_ONLINE } from "@/src/functions";
import { NextPage } from "next";
import { useRouter } from "next/router";

const Course: NextPage = () => {

  const router = useRouter();
  const {kind, buy} = router.query;
  return (
    <DefaultLayout>
      {kind==VAR_STR_ONLINE && <CourseDetailPage buy={buy} />}
      {kind==VAR_STR_DESTINATION && <TouristDetailPage />}
      {kind==VAR_STR_LIVE && <LiveDetailPage />}
    </DefaultLayout>
  );
};

export default Course

