const controller = require("../controllers/upload/course.upload.controller");
const { videoUpload, authJwt } = require("../middleware");

module.exports = function (app) {
    app.post("/api/upload/course", 
        videoUpload.fields([
            { name: 'prevfile', maxCount: 1 },
            { name: 'realfile', maxCount: 1 }
        ]),
        controller.courseUpload
    );
};