import Image from "next/image";
import { FC } from "react";

export const ImageButton: FC<{ url: string; label: string; size: number }> = ({
  url,
  label,
  size,
}) => {
  return (
    <div className="cursor-pointer flex flex-col justify-center items-center border-b-2 border-transparent hover:border-b-2 hover:border-brown-900 hover:opacity-80">
      <div className="p-1 md:p-3">
        <Image
          src={url}
          blurDataURL={url}
          width={size}
          height={size}
          alt={label}
          placeholder="blur"
        />
      </div>
      <label className="mt-2 text-center hidden sm:block">{label}</label>
    </div>
  );
};
