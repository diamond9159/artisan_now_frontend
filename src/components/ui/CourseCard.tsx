import { FC } from "react";
import Image from "next/image";
import { courseInfo, VAR_STR_DESTINATION, VAR_STR_LIVE, VAR_STR_ONLINE } from "@/src/functions";
import { OnlineIcon, DestinationIcon } from "./customIcons"
import { useTranslations } from "next-intl";
import { PlayIcon } from "./PlayIcon";


export const CourseCard: FC<{ course: courseInfo, className?: string, kind: string, buy?: string }> = (props) => {
  const t = useTranslations("user.course");
  const { imageUrl, courseName, hostName, destination, episode, duration, categories, price = "0" } = props.course;
  const kind: string = props.kind
  const buy: string = props.buy || "";
  const svgUrl = (kind == VAR_STR_ONLINE) ? OnlineIcon : DestinationIcon
  const artisanName = (kind == VAR_STR_ONLINE) ? t("subtitle.online-experiences") : t("subtitle.experiences")

  return (
    <a href={`/user/course/detail?kind=${kind}&buy=${buy}`} aria-hidden={true}>
      <div className={`flex flex-col max-w-sm h-fit rounded-[10px] hover:opacity-[0.9] ${props.className}`}>
        <div className="relative w-[320] h-[240] overflow-hidden">
          <Image
            className="object-cover rounded-[10px]"
            src={imageUrl}
            blurDataURL={imageUrl}
            width="250"
            height="150"
            style={{ width: "100%", height: "100%" }}
            alt="Course"
            loading="eager"
            placeholder="blur"
          />
          {kind != VAR_STR_DESTINATION &&
            <PlayIcon />
          }
          {kind == VAR_STR_LIVE &&
            <DescriptionOfLIVE courseName={courseName} destination={destination} price={price} />
          }
        </div>

        {kind !== VAR_STR_LIVE &&
          <div className="flex flex-col h-fit my-2">
            <h5 className="leading-[20px]">
              {courseName}
            </h5>
            <span className="mt-2">{hostName}</span>
            <span className="">{destination}</span>
            <div className="flex flex-row gap-3 items-center my-2">
              <Image
                className="rounded-[50px] ring-offset-2 ring-1 ring-brown-900"
                src={svgUrl}
                width="24"
                height="24"
                alt="ICON"
              />
              <span className="">{artisanName}</span>              
            </div>
            {kind == VAR_STR_ONLINE &&
              <div className="flex flex-col h-fit">
                <div className="flex flex-row gap-5">
                  <span>Episode: {episode}</span>
                  <span>Duration: {duration}</span>
                </div>
                <div className="flex flex-row gap-3">
                  {categories && categories.map((item, index) => (
                    <span key={index}>{item}</span>
                  ))}
                </div>
              </div>
            }
            <div className="py-2">
              <h5>${price} USD</h5>
            </div>
          </div>
        }
        
        
      </div>
    </a>
  );
};

const DescriptionOfLIVE: FC<{ courseName: string, destination: string, price: string }> = (props) => {
  return (
    <div className="flex flex-col px-2 py-1 absolute left-0 bottom-0">
      <h5 className="text-white">
        {props.courseName}
      </h5>
      <span className="text-white">{props.destination}</span>
      <span className="text-white text-600">${props.price} USD</span>
    </div>
  )

}