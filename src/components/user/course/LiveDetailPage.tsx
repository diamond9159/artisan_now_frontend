import { useTranslations } from "next-intl";
import ReactPlayer from 'react-player'
import courses from "@/public/course.json"
import { recentOnlineExperiences, recentDestinations, recentLIVE } from "../../temp"
import {
  Wrapper,
  CourseContainer,
  Container,
  CourseDetail,
  LiveCourseContainer,
  Button,
  PlayIcon,
  H3Icon,
  OnlineIcon,
} from "../../ui";
import { VAR_STR_LIVE } from "@/src/functions";

export function LiveDetailPage() {
  const t = useTranslations("user.course");
  return (
    <>
      <Wrapper>
        <Container className="w-full mx-auto aria-hidden">
          <div className="flex flex-col md:flex-row w-full gap-10 items-start course-detail">
            <div className="relative">
              <ReactPlayer className="rounded-3xl overflow-hidden"
                url='https://tecdn.b-cdn.net/img/video/Tropical.mp4'
                // light={true}
                loop={true}
                controls={true}
              ></ReactPlayer>
              <div className="bg-red-600 px-5 rounded-lg absolute right-10 top-8 z-8 border-2 border-white/[0.8]">
                <h5 className="text-white">LIVE</h5>
              </div>
            </div>
            <div className="flex flex-col">
              <h4 className="">
                Name of the Course
              </h4>
              <span className="">live chat with the artisan</span>
              <span className="">date and time</span>

              <span className="mt-2">Available languages</span>
              <span className="">price</span>

              <Button className="mt-5" label="Buy" />
            </div>
          </div>
        </Container>
      </Wrapper>
      <Wrapper>
        <Container>
          <H3Icon icon={OnlineIcon} descrpition={t("subtitle.artisan-channel-description")}>{t("subtitle.artisan-channel")}</H3Icon>
          <CourseContainer courses={recentLIVE} kind={VAR_STR_LIVE} />
        </Container>
      </Wrapper>
    </>
  );
}
