import DestinationIcon from "@/public/image/icon/experiences.svg"
import OnlineIcon from "@/public/image/icon/online_experiences.svg"
import PlayIconUrl from "@/public/image/icon/play.svg"

export {
    DestinationIcon,
    OnlineIcon,
    PlayIconUrl,
}