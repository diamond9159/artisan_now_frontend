// authService.ts
import { authHeader } from '../helpers/auth-header';
import { User } from '@/src/functions/types';
import { toast } from 'react-toastify';
import { endpoints } from '@/src/functions';

export const authService = {
  login,
  register,
  getAll,
  getById,
  update,
  delete: _delete
};

function login(username: string, password: string) {

  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username, password })
  };

  return fetch(endpoints.authSignIn, requestOptions)
    .then(handleResponse)
    .then((response) => {
      return response;
    });
}

function register(user: User) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  };
  return fetch(endpoints.authSignUp, requestOptions).then(handleResponse).then(newUser => {
    toast.success("Register successful")
  });
}

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  //return fetch(`${apiUrl}/users`, requestOptions).then(handleResponse);
}

function getById(id: number) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  //return fetch(`${apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function update(id: number, data: any) {
  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  };

  //return fetch(`${apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

// prefixed with underscore because delete is a reserved word in javascript
function _delete(id: number) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader()
  };

  //return fetch(`${apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

async function handleResponse(response: Response) {

  const isJson = response.headers?.get('content-type')?.includes('application/json');
  const data = isJson ? await response.json() : null;
  if (!response.ok) {
    if ([500, 401, 403, 400].includes(response.status)) {
   
    }    
    const err = data? data.message : response.statusText;  
    toast.error(err)  
    return Promise.reject(err);
  }

  return data;
}
