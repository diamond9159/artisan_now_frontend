/* eslint-disable no-console */
/* eslint-disable global-require */

require('dotenv').config();

const express = require("express");
const next = require('next');
const cookieSession = require("cookie-session");

const dev = process.env.DEV !== 'production';

const app = next({ dev });
const handle = app.getRequestHandler();

const server = express();

// mongoose db connect 
const db = require("./pages/api/models");
const { g_func } = require("./pages/api/global");
const { dbConfig } = require("./pages/api/config")
const Role = db.role;
const Categories = db.categories;

db.mongoose
    .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Successfully connect to MongoDB.");
        g_func.initialRole(Role);
        g_func.initialCategories(Categories);
    })
    .catch(err => {
        console.error("Connection error", err);
        process.exit();
    });

app.prepare().then(() => {
    server.use(express.json());
    server.use(express.urlencoded({ extended: true }));

    server.use(
        cookieSession({
            name: "artisanNow-session",
            keys: ["artisan_key1", "arisan_key2"],
            secret: "COOKIE_SECRET", // should use as secret environment variable
            httpOnly: true
        })
    ); 
    // routes
    const routes = require('./pages/api/routes');
    routes.authRoutes(server);
    routes.userRoutes(server);   
    routes.uploadRoutes(server);

    server.get('*', (req, res) => {
        return handle(req, res);
    });
});


// set port, listen for requests
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});