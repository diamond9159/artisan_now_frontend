import { useTranslations } from "next-intl";
import { FC, useState } from "react";
import Image from "next/image";

import { Wrapper, Container, Button, Tab } from "../../ui";
import { useRouter } from "next/router";

const ProfileSection = () => {
  const t = useTranslations("account");
  return (
    <Container className="flex flex-col md:flex-row gap-20">
      <div>
        <h2 className="text-start">{t("profile-settings")}</h2>
        <div className="user-image my-10 flex flex-wrap">
          <Image src="" alt="Avatar" width={128} height={128} />
        </div>
        <span className="text-gray-600 ">
          Tip: Tip: Choose a picture where people can see your face.
        </span>
        <br></br>
        <span className="text-gray-600 ">.JPG or .PNG. Max. 20 MB</span>
      </div>
      <div className="flex flex-col gap-3">
        <h3>Your Name</h3>
        <input
          type="text"
          name="firstname"
          placeholder="Enter your First name"
          required
        />
        <input
          type="text"
          name="lastname"
          placeholder="Enter your Last name"
          required
        />
        <h3>Your profile biography</h3>
        <textarea
          name="biography"
          aria-multiline="true"
          rows={5}
          placeholder="Tell us something about yourself..."
          maxLength={1500}
          spellCheck
        />
        <span className="text-gray-600">
          artisanNow is built on relationships. Help other people get to know
          you.
        </span>
        <Button className="w-[50%]" label={t("save-profile")} />
      </div>
    </Container>
  );
};

const AccountSection = () => {
  const router = useRouter()
  const t = useTranslations("account");

  const [selectedTab, setTab] = useState("contact");

  const go = (tabname: string) => {
    router.push(`#${tabname}`)
    setTab(tabname)
  }

  return (
    <Container className="flex flex-col ">
      <div className="flex flex-row gap-10">
        <div className="flex flex-col gap-5">
          <Tab fontsize="text-xl" label={t("tab-contact")} action={() => go("contact")} isSelected={selectedTab == "contact"} />
          <Tab fontsize="text-xl" label={t("tab-password")} action={() => go("password")} isSelected={selectedTab == "password"} />
          <Tab fontsize="text-xl" label={t("tab-delete")} action={() => go("delete")} isSelected={selectedTab == "delete"} />
          <Tab fontsize="text-xl" label={t("tab-bank")} action={() => go("bank")} isSelected={selectedTab == "bank"} />
          <Tab fontsize="text-xl" label={t("tab-contract")} action={() => go("contract")} isSelected={selectedTab == "contract"} />
          
        </div>
        <div className="flex flex-col gap-20 border-l pl-10">
          <div id="contact" className="flex flex-col gap-2">
            <h3>Your Email Address</h3>
            <input
              type="email"
              name="username"
              placeholder="Enter your Email"
              required
            />
            <span>Your email address is verified</span>

            <h3 className="mt-10">Phone Number</h3>
            <input
              className="w-[200px]"
              type="text"
              name="phonenumber"
              placeholder="Enter your Phone number"
              required
            />
            <span className="text-gray-600">
              artisanNow is built on relationships. Help other people get to
              know you.
            </span>
            <Button className="w-[50%]" label={t("save-profile")} />
          </div>

          {/* Password */}
          <div id="password" className="flex flex-col gap-2">
            <h3>New Password</h3>
            <input
              className="mb-5"
              type="password"
              name="password"
              placeholder="Enter your new Password"
              required
            />
            <input
              type="password"
              name="password"
              placeholder="Enter your new Password"
              required
            />
            <span>Please confirm the new password again.</span>
            <Button className="w-[50%]" label={t("save-profile")} />
          </div>


          {/* Account */}
          <div id="delete" className="flex flex-col gap-2">
            <h3>Delete Account</h3>

            <span>Your account will be deleted within 48 hours.</span>
            <span>If you want to delete it please click on the button below.</span>
          </div>
          <Button className="w-[50%]" label={"Delete Account"} />
        </div>
      </div>

    </Container>
  )

}

export function AccountPage() {
  const t = useTranslations("account");

  const [isProfile, setProfile] = useState<boolean>(true)

  return (
    <>
      <Wrapper className="w-full flex justify-center">
        <div className="w-[90%] lg:w-[70%]">
          <Container className="flex gap-x-4 items-center justify-end">
            <Tab label={t("profile-settings")} direction="horitical" action={() => setProfile(true)} isSelected={isProfile} />
            <Tab label={t("account-settings")} direction="horitical" action={() => setProfile(false)} isSelected={!isProfile} />
          </Container>
          {isProfile && <ProfileSection />}
          {!isProfile && <AccountSection />}
        </div>
      </Wrapper>
    </>
  );
}
