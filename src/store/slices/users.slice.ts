import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { authService } from "../services";
import { User } from "@/src/functions/types";

interface UsersState {
  users: User[];
  loading: boolean;
}

const initialState: UsersState = {
  users: [],
  loading: false,
};


export const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {

  }
});

export default usersSlice.reducer;
