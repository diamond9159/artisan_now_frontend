import { AppDispatch } from "../";
import { setLocaleSuccess } from "../slices";

export const setLocale = (newLocale: string) => (dispatch: AppDispatch) => {
    dispatch(setLocaleSuccess(newLocale));
} 