import Image from "next/image";
import { FC } from "react";

export const ImageContent: FC<{ url: string; label: string; size: number }> = ({
  url,
  label,
  size,
}) => {
  return (
    <div className="flex flex-col justify-center items-center">
      <div className="p-3">
        <Image
          src={url}
          width={size}
          height={size}
          alt={label}
          loading="lazy"
        />
      </div>      
      <p className="large">{label}</p>
    </div>
  );
};

export default ImageContent;
