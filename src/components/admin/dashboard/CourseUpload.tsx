import { useState } from "react";
import { RiAddCircleFill } from "react-icons/ri";
import { uploadService } from "@/src/store/services/upload";

interface courseInfo {
    title: string;
    hostname: string;
    destination: string;
    duration: number;
    prevfile: File | null;
    realfile: File | null;
}

export default function UploadPage() {

    const [data, setData] = useState<courseInfo>({
        title: "",
        hostname: "",
        destination: "",
        duration: 0,
        prevfile: null,
        realfile: null,
    });

    const handleFormSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        try {
            const formData: FormData = new FormData(); 
            formData.append("uuid", Date.now().toString());
            formData.append("title", data.title);
            formData.append("hostname", data.hostname);
            formData.append("destination", data.destination);
            formData.append("duration", data.duration.toString());
            
            if (data.prevfile) {
                formData.append("prevfile", data.prevfile);
            }
            if (data.realfile) {
                formData.append("realfile", data.realfile);
            }

            await uploadService.uploadCourse(formData);
                        
        } catch (error) {
            console.error(error);
        }
    };

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const field = e.target.name;
        const value = e.target.value;

        setData({
            ...data,
            [field]: value,
        });
    };

    const handlePrevVideoFileSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files ? e.target.files[0] : null;

        setData({
            ...data,
            prevfile: file,
        });
    };

    const handleRealVideoFileSelect = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files ? e.target.files[0] : null;

        const duration: any = 302803;
        setData({
            ...data,
            realfile: file,
            duration: duration
        });
    };
    
    return (
        <div className="mx-auto max-w-lg py-10 bg-white">
            <h1 className="text-2xl font-bold mb-4">Upload Video</h1>
            <form onSubmit={handleFormSubmit}>
            {/* <form method="POST" action="/api/upload/course" encType="multipart/form-data"> */}
                <div className="mb-4">
                    <label htmlFor="title" className="font-medium block mb-2">
                        Title:
                    </label>
                    <input type="text" name="title" value={data.title} onChange={handleInputChange} className="w-full border rounded py-2 px-3" />
                </div>

                <div className="mb-4">
                    <label htmlFor="hostname" className="font-medium block mb-2">
                        Hostname:
                    </label>
                    <input type="text" name="hostname" value={data.hostname} onChange={handleInputChange} className="w-full border rounded py-2 px-3" />
                </div>

                <div className="mb-4">
                    <label htmlFor="destination" className="font-medium block mb-2">
                        Destination:
                    </label>
                    <input type="text" name="destination" value={data.destination} onChange={handleInputChange} className="w-full border rounded py-2 px-3" />
                </div>

                <div className="mb-4">
                    <label htmlFor="category" className="font-medium block mb-2">
                        Category:
                    </label>
                    <select name="category" onChange={handleInputChange} className="w-full border rounded py-2 px-3">
                        <option value="Online Experiences">
                            Online Experiences
                        </option>
                        <option value="Experiences and Destinations">
                            Experiences and Destinations
                        </option>   
                    </select>
                </div>             

                <div className="mb-4">
                    <label htmlFor="prevVideofile" className="font-medium block mb-2">
                        Preview Video File:
                    </label>

                    <label htmlFor="prevVideoFileInput" className="flex items-center justify-center border border-dashed rounded-md p-4 cursor-pointer hover:bg-gray-100">
                        {data.prevfile ? (
                            <div className="mr-2">{data.prevfile.name}</div>
                        ) : (
                                <RiAddCircleFill className="h-8 w-8 mr-2 text-blue-500" />
                        )}
                        <span className="text-gray-400">Upload a video file</span>

                        <input id="prevVideoFileInput" name="prevfile" type="file" className="hidden" accept=".mp4" onChange={handlePrevVideoFileSelect} />
                    </label>
                </div>

                <div className="mb-4">
                    <label htmlFor="realVideofile" className="font-medium block mb-2">
                        Real Video File:
                    </label>

                    <label htmlFor="realVideoFileInput" className="flex items-center justify-center border border-dashed rounded-md p-4 cursor-pointer hover:bg-gray-100">
                        {data.realfile ? (
                            <div className="mr-2">{data.realfile.name}</div>
                        ) : (
                                <RiAddCircleFill className="h-8 w-8 mr-2 text-blue-500" />
                        )}
                        <span className="text-gray-400">Upload a video file</span>

                        <input id="realVideoFileInput" name="realfile" type="file" className="hidden" accept=".mp4" onChange={handleRealVideoFileSelect} />
                    </label>
                </div>

                <button type="submit" className="px-4 py-2 bg-blue-500 text-white font-medium rounded-md hover:bg-blue-600">Upload</button>
            </form>
        </div>
    );
}